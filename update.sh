#!/usr/bin/bash

spectool -fg containers-common.spec

echo "Changing storage.conf..."
sed -i -e 's/^driver.*=.*/driver = "overlay"/' -e 's/^mountopt.*=.*/mountopt = "nodev,metacopy=on"/' \
        storage.conf

echo "Changing seccomp.json..."
[ `grep "keyctl" seccomp.json | wc -l` == 0 ] && sed -i '/\"kill\",/i \
                                "keyctl",' seccomp.json
sed -i '/\"socketcall\",/i \
                                "socket",' seccomp.json

echo "Changing registries.conf..."
sed -i 's/^#.*unqualified-search-registries.*=.*/unqualified-search-registries = ["registry.fedoraproject.org", "registry.access.redhat.com", "docker.io"]/g' \
        registries.conf

echo "Changing containers.conf..."
sed -i -e 's/^#.*log_driver.*=.*/log_driver = "k8s-file"/' containers.conf
